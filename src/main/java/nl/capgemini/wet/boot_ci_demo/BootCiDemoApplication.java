package nl.capgemini.wet.boot_ci_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootCiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootCiDemoApplication.class, args);
	}

}
