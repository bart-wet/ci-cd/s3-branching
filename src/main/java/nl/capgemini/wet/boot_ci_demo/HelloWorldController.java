package nl.capgemini.wet.boot_ci_demo;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {

    @GetMapping("/")
    public String sayHello(final Model model, @RequestParam("name") final String name) {
        if (name == null) {
            return "empty";
        }
        final String temp = Jsoup.clean(name, Whitelist.none());
        model.addAttribute("name", temp);
        return "hello";
    }

}
